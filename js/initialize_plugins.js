//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var select2    = $("select.styler"),
			selectSearch2    = $("select.styler2"),
			matchheight = $("[data-mh]"),
			dragg = $(".dragg"),
		    styler = $(".styler:not(select)"),
		    scroll = $(".scroll"),
			popup = $("[data-popup]"),
			datepickerCarousel  = $('.k-datepicker-range'),
			windowW = $(window).width(),
			windowH = $(window).height();


			if(scroll.length){
					include("plugins/jquery.nicescroll.min.js");
					// include("plugins/jquery.mousewheel.js");
			}
			if(select2.length || selectSearch2){
					include("plugins/select2.min.js");
					include("plugins/jquery.nicescroll.min.js");
					// include("js/jquery.mousewheel.shim.js");
					// include("js/mwheelIntent.js");
			}
			if(matchheight.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(datepickerCarousel.length){
					include("plugins/jquery-ui.min.js");
					include("plugins/owl.carousel.js");
			}
			if(dragg.length){
					include("plugins/jquery-ui.min.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}

					include("plugins/modernizr.js");



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){

			/* ------------------------------------------------
			SELECT2 WITH SKROLL START
			------------------------------------------------ */

				if (select2.length){
					// select2.select2();

					select2.each(function(){
				        var ret     = $(this).select2({
				        	minimumResultsForSearch: -1
				        });
				        // custom scrollbars
				        var s;
				        ret.on("select2-open", function () {
				            if (!s) {
				                s = $('.select2-drop-active > .select2-results');
				                s.niceScroll({
				                    autohidemode: false,
				                    cursorcolor: "#adb1b6",
				                    cursorwidth: 5,
				                    cursorborderradius: "0"
				                });
				            }
				        });
				    });

				}

				if (selectSearch2.length){
					// select2.select2();

					selectSearch2.each(function(){
				        var ret     = $(this).select2({});
				        // custom scrollbars
				        var s;
				        ret.on("select2-open", function () {
				            if (!s) {
				                s = $('.select2-drop-active > .select2-results');
				                s.niceScroll({
				                    autohidemode: false,
				                    cursorcolor: "#adb1b6",
				                    cursorwidth: 5,
				                    cursorborderradius: "0"
				                });
				            }
				        });
				    });

				}

			/* ------------------------------------------------
			SELECT2 WITH SKROLL END
			------------------------------------------------ */

			/* ------------------------------------------------
			SCROLL START
			------------------------------------------------ */

					if (scroll.length){
						scroll.niceScroll({
							cursorcolor: '#a6acb2'
						});
					};

			/* ------------------------------------------------
			SCROLL END
			------------------------------------------------ */


			/* ------------------------------------------------
			MATCHHEIGHT START
			------------------------------------------------ */

					if (matchheight.length){
						$(matchheight).matchHeight();
					};

			/* ------------------------------------------------
			MATCHHEIGHT END
			------------------------------------------------ */

			/* ------------------------------------------------
			DRAGG START
			------------------------------------------------ */

					if (dragg.length){
						dragg.sortable({
							revert: true
						});
					};

			/* ------------------------------------------------
			DRAGG END
			------------------------------------------------ */

			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */

			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal();
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */


			if (datepickerCarousel.length){

			     $('body').append('<div id="dpCarouselNav"></div>');

			     var dpWidth = 0;

			     function appendDpW(width){
			       $('head').append('<style>.with-custom-nav-widget{width:'+width+'px !important}</style>');
			     }

			    datepickerCarousel.datepicker({
			       autoSize: true,
			       changeMonth: true,
			       prevText: ' ',
			       nextText: ' ',
			          changeYear: true,
			          dayNamesMin: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
			          beforeShow : function(input, inst){

			         $(inst.dpDiv).addClass('with-custom-nav-widget');

			         $("#dpCarouselNav").fadeIn(100);

			         setTimeout(function(){
			           var currentDpDiv = $(inst.dpDiv),
			             currentDpDivOffset = currentDpDiv.offset();

			           dpWidth = $(input).outerWidth();

			           appendDpW(dpWidth);

			           currentDpDiv.css({
			             'width' : dpWidth
			           });

			           $("#dpCarouselNav").css({
			             'width' : dpWidth,
			             "top"   : currentDpDivOffset.top,
			             "left"  : currentDpDivOffset.left
			           });

			           if (currentDpDiv.find('select').length && !$("#dpCarouselNav").find("ul").length){
			             currentDpDiv.find('select').each(function(){
			               var currentSelect     = $(this),
			                  currentSelectClass   = currentSelect.attr('class'),
			                 ul           = '<ul data-dp-list="'+currentSelectClass+'">';

			               currentSelect.find('option').each(function(){
			                 var currentOption = $(this),
			                   currentVal    = currentOption.text(),
			                   isSelected     = currentOption.attr('selected');

			                 if (isSelected){
			                   ul+='<li class="selected">'+currentVal+'</li>';
			                 }
			                 else{
			                   ul+='<li>'+currentVal+'</li>';
			                 }
			               })
			               ul+='</ul>';

			               $("#dpCarouselNav").prepend(ul);

			             })
			           }

			           $("#dpCarouselNav").on('click', 'ul[data-dp-list="ui-datepicker-month"] li', function(){
			             var currentLi = $(this),
			               parentUl = currentLi.closest('ul'),
			               parentUlClass = parentUl.attr('data-dp-list'),
			               currentliIndex = currentLi.parent().index();

			             parentUl.find('li').removeClass('selected');
			             currentLi.addClass('selected');

			             currentDpDiv.find('select.'+parentUlClass+'').val(currentliIndex).trigger('change');

			           })

			           $("#dpCarouselNav").on('click', 'ul[data-dp-list="ui-datepicker-year"] li', function(){
			             var currentLi = $(this),
			               parentUl = currentLi.closest('ul'),
			               parentUlClass = parentUl.attr('data-dp-list'),
			               currentliIndex = currentLi.text();

			             parentUl.find('li').removeClass('selected');
			             currentLi.addClass('selected');

			             currentDpDiv.find('select.'+parentUlClass+'').val(currentliIndex).trigger('change');

			           })

			           $("#dpCarouselNav").find('ul').each(function(){
			             var current = $(this);

			             current.owlCarousel({
			               navigation : true,
			               rewindNav : false,
			              navigationText : ['<i class="glyphicon glyphicon-chevron-left"></i>','<i class="glyphicon glyphicon-chevron-right"></i>'],
			             });

			             current.data('owlCarousel').jumpTo(current.find('li.selected').parent().index() - 2);
			           })


			         }, 50);

			       },
			       onClose : function(text, inst){
			         $("#dpCarouselNav").fadeOut(150);
			         $(inst.dpDiv).removeClass('with-custom-nav-widget');
			       }
			     });
			   }


		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
